/*
 * pep.cxx
 *
 *  Created on: Oct 19, 2018
 *      Author: vito
 */

#include "pep.hxx"

#include <algorithm>
#include <array>
#include <exception>
#include <iostream>
#include <sstream>
#include <stdexcept>
#include <vector>

#include <SDL2/SDL.h>

namespace pep {

///Known pep::events must be stringified here
///
std::array<std::string_view, 17> bindings::event_names = {
    { /// input events
        "left_pressed", "left_released", "right_pressed", "right_released",
        "up_pressed", "up_released", "down_pressed", "down_released",
        "select_pressed", "select_released", "start_pressed", "start_released",
        "button1_pressed", "button1_released", "button2_pressed",
        "button2_released",
        /// virtual console events
        "turn_off" }
};

///Bind SDL defined keys with pep::events
///
const std::array<bindings::bind, 8> bindings::keys{
    { { SDLK_w, "up", event::up_pressed, event::up_released },
        { SDLK_a, "left", event::left_pressed, event::left_released },
        { SDLK_s, "down", event::down_pressed, event::down_released },
        { SDLK_d, "right", event::right_pressed, event::right_released },
        { SDLK_LCTRL, "button1", event::button1_pressed,
            event::button1_released },
        { SDLK_SPACE, "button2", event::button2_pressed,
            event::button2_released },
        { SDLK_ESCAPE, "select", event::select_pressed, event::select_released },
        { SDLK_RETURN, "start", event::start_pressed, event::start_released } }
};

///Supporting only known pep::events
///
std::ostream& operator<<(std::ostream& stream, const event e)
{
    std::uint32_t value = static_cast<std::uint32_t>(e);
    std::uint32_t minimal = static_cast<std::uint32_t>(event::left_pressed);
    std::uint32_t maximal = static_cast<std::uint32_t>(event::turn_off);
    if (value >= minimal && value <= maximal) {
        stream << bindings::event_names[value];
        return stream;
    } else {
        throw std::runtime_error("pep::operator<< : too big event value");
    }
}

///Supporting only known input pep::events
///
static bool check_input(const SDL_Event& e, const bindings::bind*& result)
{
    using namespace std;

    const auto it = find_if(begin(bindings::keys), end(bindings::keys), [&](const bindings::bind& b) {
        return b.key == e.key.keysym.sym;
    });

    if (it != end(bindings::keys)) {
        result = &(*it);
        return true;
    }
    return false;
}

///SDL version control
///
static std::ostream& operator<<(std::ostream& out, const SDL_version& v)
{
    out << static_cast<int>(v.major) << '.';
    out << static_cast<int>(v.minor) << '.';
    out << static_cast<int>(v.patch);
    return out;
}

///primitive engine himselfs
///
class pengine_impl final : public pengine {
private:
    SDL_Window* window;

public:
    /// create main window
    /// on success return empty string
    ///
    std::string initialize(std::string_view /*config*/) final
    {
        using namespace std;

        stringstream serr;

        SDL_version compiled = { 0, 0, 0 };
        SDL_version linked = { 0, 0, 0 };

        SDL_VERSION(&compiled);
        SDL_GetVersion(&linked);

        if (SDL_COMPILEDVERSION != SDL_VERSIONNUM(linked.major, linked.minor, linked.patch)) {
            serr << "warning: SDL2 compiled and linked version mismatch: "
                 << compiled << " " << linked << endl;
        }

        const int init_result = SDL_Init(SDL_INIT_EVERYTHING);
        if (init_result != 0) {
            const char* err_message = SDL_GetError();
            serr << "error: failed call SDL_Init: " << err_message << endl;
            return serr.str();
        }

        if (window != nullptr) {
            SDL_DestroyWindow(window);
        }
        window = SDL_CreateWindow(
            "title", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 640, 480,
            ::SDL_WINDOW_OPENGL);

        if (window == nullptr) {
            const char* err_message = SDL_GetError();
            serr << "error: failed call SDL_CreateWindow: " << err_message
                 << endl;
            SDL_Quit();
            return serr.str();
        }
        return "";
    }
    ///
    /// pool event from input queue
    ///
    bool read_input(event& e) final
    {
        using namespace std;
        // collect all events from SDL
        SDL_Event sdl_event;
        if (SDL_PollEvent(&sdl_event)) {
            const bindings::bind* binding = nullptr;

            if (sdl_event.type == SDL_QUIT) {
                e = event::turn_off;
                return true;
            } else if (sdl_event.type == SDL_KEYDOWN) {
                if (check_input(sdl_event, binding)) {
                    e = binding->event_pressed;
                    return true;
                }
            } else if (sdl_event.type == SDL_KEYUP) {
                if (check_input(sdl_event, binding)) {
                    e = binding->event_released;
                    return true;
                }
            }
        }
        return false;
    }
    ///
    /// finish all this
    ///
    void uninitialize() final
    {
        SDL_DestroyWindow(window);

        SDL_Quit();
    }
};

static bool already_exist = false;

/// init after maybe?
///
pengine* create_engine()
{
    if (already_exist) {
        throw std::runtime_error("engine already exist");
    }
    pengine* result = new pengine_impl();
    already_exist = true;
    return result;
}

/// uninit before maybe?
///
void destroy_engine(pengine* e)
{
    if (already_exist == false) {
        throw std::runtime_error("engine not created");
    }
    if (nullptr == e) {
        throw std::runtime_error("e is nullptr");
    }
    delete e;
}

pengine::~pengine() {}

} /* namespace pep */
